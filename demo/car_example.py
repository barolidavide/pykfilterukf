import numpy as np
from pykfilter import ReducedSimplexUnscentedKalmanFilter
from matplotlib import pyplot as plt
from itertools import izip

"""
We estimate the constant velocity of a car moving along an 1D road.
The state of the car is modeled as the 2-D vector [[position],
                                                   [velocity]],
and our observations are noisy measurements of position.

We assume that our initial covariance matrix has the form
[[0, 0],
 [0, s]], i.e. that we are virtually certain about our guess for the
position of the car, and factor it as L*U.I*L.T with
L = [[0, 1]] and U = [[s^-1]].


"""


# The transition function of the system. The timestep n is taken
# an argument so pykfilter can handle time dependence,
# but in this case is not relevant. The argument i is the number of
# the sigma point, and can also safely be ignored unless you want to
# do something special with the sampled sigma points.

def transition_function(state, n, i):
    dt = 0.1
    F = np.matrix([[1, dt],
                   [0, 1]])
    return F * state

def observation_function(state, n, i):
    return state[:1, 0]


def gauss_noise(covar):
    # Note that while we here use Gaussian noise,
    # the filter does not require us to.

    s = covar.shape[0]
    noise = np.random.multivariate_normal(np.zeros(s), covar)
    return np.matrix([noise]).T

# Standard deviation of noise.
s_z = 1.0
observation_noise_covar = np.matrix([[s_z**2]])

# Actual initial state.
v = 3.0
state = np.matrix([[0],
                   [v]])

N = 100
state_list = [state]
obs_list = []

for i in range(N):
    state = transition_function(state, i, -1)
    state_list.append(state)

    obs = (observation_function(state, i, -1) +
           gauss_noise(observation_noise_covar))
    obs_list.append(obs)

"""
This is where use of pykfilter starts.
"""

# The filter needs to know the transition and observation functions.
# The argument 1 is the size of U in the covariance matrix factorization.

rukf = ReducedSimplexUnscentedKalmanFilter(transition_function,
                                           observation_function,
                                           observation_noise_covar.I, 1)

init_v_est = 1.0
init_state_est = np.matrix([[0],
                            [init_v_est]])

L_init = np.matrix([[0], [1]])

# Note that the factorization we use is L*U.I*L.T and not L*U*L.T.
# This is to remain compatible with Moireau and Chapelle.

init_velocity_std = 2.0
U_init = np.matrix([[init_velocity_std**-2]])


est_list, _, L_list, U_inverse_list = rukf.filter(init_state_est,
                                                  L_init, U_init,
                                                  obs_list, get_LU = True)

vel_est_list = [est[1, 0] for est in est_list]


from pykfilter.utils import CovarianceMatrixDecomposition

vel_std_list = [CovarianceMatrixDecomposition(L, Ui).get(1, 1)
                for L, Ui in izip(L_list, U_inverse_list)]

# The preceding line is equivalent to the following, with the exception
# that CovarianceMatrixDecomposition only computes the entries of L*Ui*L^T
# that get() is called on.

# vel_std_list = [(L*Ui*L.T)[1, 1] for L, Ui in izip(L_list, U_inverse_list)]


"""
Plotting
"""

fig = plt.figure()
ax = fig.gca()
ax.set_title("Estimate of velocity")

ax.set_ylim(bottom=0.8 * min(v, min(vel_est_list)),
            top=1.2 * max(v, max(vel_est_list)))
ax.set_xlim(left=0, right=N)

plt.plot(range(N + 1), [v] * (N + 1), "g--")
ax.errorbar(range(N + 1), vel_est_list, yerr=vel_std_list,
            ecolor="r", elinewidth=0.3)

plt.show()
