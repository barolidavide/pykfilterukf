import numpy as np
from numpy.linalg import cholesky

from .sigma_points import SimplexSigmaPoints

class ReducedSimplexUnscentedKalmanFilter(object):
    """
    A simplex-based rUKF using simplex sampling,
    following section 3 of Moireau, Chapelle (2010).

    :parameters:
        transition_function:
            the evolution function of the system, so that the
            successor of x at observation number n -> n + 1 is F(x, n)
        observation_function:
            the function H sucj that the observation of
            a state x at time n is H(x, n) + noise
        observation_noise_inverse:
            the inverse of the covariance matrix of noise
        reduced_rank:
            the reduced rank of the system
    """

    def __init__(self, transition_function, observation_function,
                 observation_noise_inverse, reduced_rank):

        self.transition_function = transition_function
        self.observation_function = observation_function
        self.reduced_rank = reduced_rank
        self.simplex_points = SimplexSigmaPoints(reduced_rank)

        # Maybe this should be moved to the SigmaPoint class
        V_right = np.matrix([[-1] for i in range(reduced_rank)])
        V_left = np.eye(reduced_rank)
        self.V = np.matrix(np.hstack((V_left, V_right)))

        D = np.matrix(np.diag(self.simplex_points.weights))
        self.PaV = self.V * D * self.V.T

        self.observation_noise_inverse = observation_noise_inverse


    def sample(self, state, Ui, L):
        """
        Returns simplex points sampled around a given state
        using the covariance matrix defined by L * Ui * L^T.

        :parameters:
            state:
                the state around which points are sampled
            Ui:
                one half of the covariance matrix factorization
            L:
                the other half of the covariance matrix factorization
        """

        C = cholesky(Ui)
        sample_func = lambda p, i: state + L * C * p
        sampled_points = self.simplex_points.propagate(sample_func)
        return sampled_points

    def predict(self, n, sampled_points):
        """
        Returns the prediction for state at observation number n + 1 as sigma points.

        :parameters:
            n:
                current observation number
            sampled_points:
                sampling of points around current state estimate
        """

        return sampled_points.propagate(lambda p, i:
                                        self.transition_function(p, n, i))

    def correct(self, n, pred_sigmas, actual_obs, get_relerr=False):
        """
        Returns updated estimate of state and its covariance matrix
        decomposition, and (optionally) relative error of predicted observation.

        :parameters:
            n:
                current observation number
            pred_sigmas:
                sigma points encoding state prediction
            actual_obs:
                observation made of system
            get_relerr:
                if set, returns relative error of predicted observation
        """

        state_pred = pred_sigmas.weighted_mean()
        obs_sigmas = pred_sigmas.propagate(lambda p, i:
                                           self.observation_function(p, n + 1, i))
        obs_err = actual_obs - obs_sigmas.weighted_mean()
        Z = np.hstack(obs_sigmas.points)
        X = np.hstack(pred_sigmas.points)
        Wi = self.observation_noise_inverse
        D = np.matrix(np.diagflat(pred_sigmas.weights))

        # DVt, HLtWi are saved to avoid redundant computation
        DVt = D * self.V.T
        L = X * DVt
        HL = Z * DVt
        HLtWi = HL.T * Wi
        Ui = (self.PaV + HLtWi * HL).I

        # the correction is computed in a roundabout manner to ensure
        # we don't compute the (potentially) huge matrix L*Ui*HLtWi
        correction = HLtWi * obs_err
        correction = Ui * correction
        correction = L * correction
        state_est_update = state_pred + correction

        if get_relerr:
            relerr = np.linalg.norm(obs_err) / np.linalg.norm(actual_obs)
            return state_est_update, L, Ui, relerr
        else:
            return state_est_update, L, Ui

    def filter(self, init_state_est, L_init, U_init,
               obs_list, get_LU=False, get_relerr=False):

        """
        Returns list of state estimates and predictions, as
        well as (optionally) factored estimate covariances
        and relative error of predicted observations.

        :parameters:
            init_state_est:
                initial state estimate
            L_init:
                half of the decomposition of initial estimate covariance
            U_init:
                the other half of initial estimate covariance decomposition
            obs_list:
                list of observations
            get_LU:
                if set, returns lists of estimate covariance decompositions
            get_err:
                if set, returns list of rel. error of predicted observations
        """

        state_est_list = [init_state_est]
        state_pred_list = [None]
        U = np.matrix(U_init)
        Ui = U.I
        L = np.matrix(L_init)

        if get_LU:
            llist = [L]
            uilist = [Ui]
        if get_relerr:
            errnormlist = [None]

        for n in range(len(obs_list)):
            prev_state_est = state_est_list[-1]

            # Sample
            sigma_points = self.sample(prev_state_est, Ui, L)
            # Predict
            prop_sigmas = self.predict(n, sigma_points)
            state_pred_list.append(prop_sigmas.weighted_mean())

            # Correct
            new_est, L, Ui, relerr = self.correct(n, prop_sigmas,
                                                  obs_list[n], get_relerr=True)

            state_est_list.append(new_est)

            if get_relerr:
                errnormlist.append(relerr)
            if get_LU:
                llist.append(L)
                uilist.append(Ui)

        ret = FilterResult(state_est_list, state_pred_list)
        #ret = [state_est_list, state_pred_list]
        if get_LU:
            ret.llist = llist
            ret.uilist = uilist
        #    ret.extend([llist, uilist])
        if get_relerr:
            ret.errnormlist= errnormlist
            #ret.append(errnormlist)
        return ret
    
class FilterResult(object):
    def __init__(self, state_estimates, state_predictions,
                 llist = None, uilist = None, errnormlist = None):
        self.state_estimates = state_estimates
        self.state_predictions = state_predictions
        self.llist = llist
        self.uilist = uilist
        self.errnormlist = errnormlist