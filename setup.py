from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name="pykfilter",
      version="0.1",
      description="""
      Python implementation of a reduced
      unscented Kalman filter.
      """,
      long_description=readme(),
      classifiers=[
          "Programming Language :: Python :: 2.7"
      ],
      keywords="reduced kalman filter rukf",
      url="https://bitbucket.org/karl0erik/pykfilter",
      author="Karl Erik Holter",
      author_email="karl0erik@gmail.com",
      license="GPL3",
      packages=['pykfilter',
                'pykfilter.utils',
                'tests'],
      install_requires=['numpy'])
