import numpy as np
from pykfilter.utils import CovarianceMatrixDecomposition as CMD


MINI_EPS = 1.0e-16
def test_covariance_matrix_decomposition():
    L = np.matrix([[1, 2],
                   [3, 4],
                   [5, 6]])
    U = np.matrix([[7, 8],
                    [9, 10]])

    decomp = CMD(L, U.I)
    P = L * U.I * L.T

    for i in range(2):
        for j in range(2):
            assert (decomp.get(i, j) - P[i, j]) < MINI_EPS
