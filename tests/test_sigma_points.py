from pykfilter.sigma_points import SigmaPoints, SimplexSigmaPoints

import numpy as np


#todo: parameterize this over sigma point methods

EPS = 1e-4
MINI_EPS = 1.0e-16
def _sigma_point_test_set():
    points = [np.matrix([[3], [1], [4]]), np.matrix([[2], [7], [1]]),
              np.matrix([[8], [2], [8]]), np.matrix([[1], [0], [0]])]
    weights = [0.9, 0.5, 0.6, 0.7]
    return SigmaPoints(points, weights)



def test_weighted_mean():
    sigma_points = _sigma_point_test_set()
    assert np.allclose(sigma_points.weighted_mean (),
                       np.matrix([[9.2], [5.6], [8.9]]), atol=MINI_EPS)
def test_weighted_covar():
    sigma_points = _sigma_point_test_set()
    assert np.allclose(sigma_points.weighted_covar(),
                       np.matrix([[108.448, 55.364, 107.516],
                                 [55.364, 49.752, 51.588],
                                 [107.516, 51.588, 108.747]]), atol=MINI_EPS)


def test_simplex_points():
    from math import sqrt
    alpha = 0.25
    actual_points =  [np.matrix([[1 / sqrt(2 * alpha)],
                                [1 / sqrt(6 * alpha)],
                                [1 / sqrt(12 * alpha)]]),
                      np.matrix([[-1 / sqrt(2 * alpha)],
                                [1 / sqrt(6 * alpha)],
                                [1 / sqrt(12 * alpha)]]),
                      np.matrix([[0],
                                [-2 / sqrt(6 * alpha)],
                                [1 / sqrt(12 * alpha)]]),
                      np.matrix([[0],
                                [0],
                                [-3 / sqrt(12 * alpha)]])]
    simplex_points = SimplexSigmaPoints(3)

    assert np.allclose(actual_points, simplex_points.points)


def test_simplex_weights():
    simplex_points = SimplexSigmaPoints(7)
    assert ([0.125] * 8 == simplex_points.weights)


def test_simplex_points_covariance():
    simplex_points = SimplexSigmaPoints(4)

    assert np.allclose(simplex_points.weighted_covar(), np.eye(4))

def test_simplex_points_mean():
    simplex_points = SimplexSigmaPoints(5)

    assert np.allclose(simplex_points.weighted_mean(), np.zeros((5, 1)))
